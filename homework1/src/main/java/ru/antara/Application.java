package ru.antara;

import ru.antara.model.Kotik;

public class Application {

    public static void main(String[] args) {
        Kotik firstKotik = new Kotik(3, "Кошка", 2, "Мур", 1);
        Kotik secondKotik = new Kotik();
        secondKotik.setKotik(2, "Кот", 3, "Мяу", 2);
        firstKotik.liveAnotherDay();
        System.out.println("Котик " + firstKotik.getName() + " весит " + firstKotik.getWeight() + " кг");
        if (firstKotik.getMeowVoice().equals(secondKotik.getMeowVoice())) {
            System.out.println("Котики мяукают одинаково");
        } else {
            System.out.println("Котики мяукают по разному");
        }
        System.out.println("В результате работы программы было создано " + Kotik.getQuantityInstances() + " котика");
    }

}
