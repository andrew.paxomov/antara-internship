package ru.antara.model;

import java.util.Random;

public class Kotik {
    private static int quantityInstances;
    private int prettiness;
    private String name;
    private int weight;
    private String meowVoice;
    private final int minSatietyValue = 0; // 0 - полностью голодный кот.
    private final int maxSatietyValue = 10; // 10 - полностью сытый кот
    private int satietyValue;

    public Kotik() {
        addInstance();
    }

    public Kotik(int prettiness, String name, int weight, String meowVoice, int satietyValue) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meowVoice = meowVoice;
        this.satietyValue = satietyValue;
        addInstance();
    }

    private static void addInstance() {
        quantityInstances++;
    }

    public static int getQuantityInstances() {
        return quantityInstances;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public String getMeowVoice() {
        return meowVoice;
    }

    public int getSatietyValue() {
        return satietyValue;
    }

    private boolean play() {
        if (satietyValue <= minSatietyValue) return false;
        System.out.println("Котик " + this.name + " играет");
        return true;
    }

    private boolean sleep() {
        if (satietyValue <= minSatietyValue) return false;
        System.out.println("Котик " + this.name + " спит");
        return true;
    }

    private boolean chaseMouse() {
        if (satietyValue <= minSatietyValue) return false;
        System.out.println("Котик " + this.name + " ловит мышь");
        return true;
    }

    private boolean meow() {
        if (satietyValue <= minSatietyValue) return false;
        System.out.println("Котик " + this.name + " мяукает");
        return true;
    }

    private boolean eat(int satietyValue) {
        if ((this.satietyValue + satietyValue) > maxSatietyValue) {
            System.out.println("Котик " + this.name + " сыт");
            return false;
        }
        this.satietyValue += satietyValue;
        System.out.println("Котик " + this.name + " ест");
        return true;
    }

    private boolean eat(int satietyValue, String food) {
        if ((this.satietyValue + satietyValue) > maxSatietyValue) {
            System.out.println("Котик " + this.name + " сыт");
            return false;
        }
        this.satietyValue += satietyValue;
        System.out.println("Котик " + this.name + " ест " + food);
        return true;
    }

    private boolean eat() {
        return eat(1, "мясо");
    }

    public void setKotik(int prettiness, String name, int weight, String meowVoice, int satietyValue) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meowVoice = meowVoice;
        this.satietyValue = satietyValue;
    }

    public void liveAnotherDay() {
        for (int i = 0; i < 24; i++) {
            int randomChoice = new Random().nextInt(7) + 1;
            switch (randomChoice) {
                case 1:
                    if (!play()) eat(1);
                    break;
                case 2:
                    if (!sleep()) eat(1);
                    break;
                case 3:
                    if (!chaseMouse()) eat(1);
                    break;
                case 4:
                    if (!meow()) eat(1);
                    break;
                case 5:
                    eat(1);
                    break;
                case 6:
                    eat(1, "рыбу");
                    break;
                case 7:
                    eat();
                    break;
            }
        }
    }
}
