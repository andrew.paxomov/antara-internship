package ru.antara.personal;

import ru.antara.animals.Animal;
import ru.antara.animals.Carnivorous;
import ru.antara.animals.Voice;
import ru.antara.enclosure.Enclosure;
import ru.antara.enclosure.Size;
import ru.antara.exception.WrongFoodException;
import ru.antara.food.Food;

public class Worker {

    public void feed(Animal animal, Food food) {
        try {
            animal.eat(food);
        } catch (WrongFoodException ex) {
            ex.printStackTrace();
        }
    }

    public void getVoice(Voice animal) {
        System.out.println(animal.voice());
    }

    public Enclosure<Carnivorous> createCarnivorousEnclosure(Size enclosureSize) {
        System.out.println("Создаём вольер для хищников размером " + enclosureSize.getName());
        Enclosure<Carnivorous> enclosure = new Enclosure<>(enclosureSize);
        return enclosure;
    }

    public void addAnimalsToEnclosure(Enclosure enclosure, Animal animal) {
        enclosure.addAnimal(animal);
    }

}
