package ru.antara.personal;

import ru.antara.animals.Carnivorous;
import ru.antara.animals.Herbivore;
import ru.antara.enclosure.Enclosure;
import ru.antara.enclosure.Size;

public class Manager {
    private Enclosure<Carnivorous> carnivorousEnclosure;
    private Enclosure<Herbivore> herbivoreEnclosure;

    public void createCarnivorousEnclosure(Size enclosureSize) {
        System.out.println("Создаём вольер для хищников размером " + enclosureSize.getName());
        carnivorousEnclosure = new Enclosure<>(enclosureSize);
    }

    public void createHerbivoreEnclosureEnclosure(Size enclosureSize) {
        System.out.println("Создаём вольер для травоядных размером " + enclosureSize.getName());
        herbivoreEnclosure = new Enclosure<>(enclosureSize);
    }

    public void addCarnivorousAnimal(Carnivorous animal) {
        carnivorousEnclosure.addAnimal(animal);
    }

    public void addHerbivoreAnimal(Herbivore animal) {
        herbivoreEnclosure.addAnimal(animal);
    }

    public void deleteCarnivorousAnimal(Carnivorous animal) {
        carnivorousEnclosure.deleteAnimal(animal);
    }

    public void deleteHerbivoreAnimal(Herbivore animal) {
        herbivoreEnclosure.deleteAnimal(animal);
    }

    public void searchCarnivorousAnimal(String carnivorousAnimalToSearch) {
        try {
            System.out.println("Ищем в вольере для хищников животное " + carnivorousAnimalToSearch);
            System.out.println("Животное " + carnivorousEnclosure.getAnimal(carnivorousAnimalToSearch).getName()
                    + " есть в вольере для хищников");
        } catch (NullPointerException e) {
            System.out.println("Животного " + carnivorousAnimalToSearch + " в вольере для хищников нет");
        }
    }

    public void searchHerbivoreAnimal(String herbivoreAnimalToSearch) {
        try {
            System.out.println("Ищем в вольере для травоядных животное " + herbivoreAnimalToSearch);
            System.out.println("Животное " + herbivoreEnclosure.getAnimal(herbivoreAnimalToSearch).getName()
                    + " есть в вольере для травоядных");
        } catch (NullPointerException e) {
            System.out.println("Животного " + herbivoreAnimalToSearch + " в вольере для травоядных нет");
        }
    }

}
