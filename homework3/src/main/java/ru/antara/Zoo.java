package ru.antara;

import ru.antara.animals.*;
import ru.antara.enclosure.Size;
import ru.antara.food.*;
import ru.antara.personal.Manager;
import ru.antara.personal.Worker;

public class Zoo {
    private final Duck duck = new Duck();
    private final Wolf wolf = new Wolf();
    private final Fish fish = new Fish();
    private final Horse horse = new Horse();
    private final Lion lion = new Lion();
    private final Rabbit rabbit = new Rabbit();

    private final Alfalfa alfalfa = new Alfalfa();
    private final Beef beef = new Beef();
    private final Oatmeal oatmeal = new Oatmeal();
    private final Sheepmeat sheepmeat = new Sheepmeat();
    private final Clover clover = new Clover();
    private final Pork pork = new Pork();

    private final Worker worker = new Worker();
    private final Manager manager = new Manager();

    public static void main(String[] args) {
        Zoo zoo = new Zoo();
        zoo.feedAll();
        zoo.getVoices();
        zoo.letUsSwim();
        zoo.сarnivorousWork();
        zoo.herbivoreWork();
    }

    private void herbivoreWork() {
        manager.createHerbivoreEnclosureEnclosure(Size.SMALL);
        manager.addHerbivoreAnimal(duck);
        manager.addHerbivoreAnimal(horse);
        manager.addHerbivoreAnimal(rabbit);
        manager.deleteHerbivoreAnimal(duck);
        manager.searchHerbivoreAnimal("Лошадь");
        manager.searchHerbivoreAnimal("Кролик");
    }

    private void сarnivorousWork() {
        manager.createCarnivorousEnclosure(Size.MEDIUM);
        manager.addCarnivorousAnimal(wolf);
        manager.addCarnivorousAnimal(lion);
        manager.addCarnivorousAnimal(fish);
        manager.deleteCarnivorousAnimal(fish);
        manager.searchCarnivorousAnimal("Волк");
        manager.searchCarnivorousAnimal("Рыба");
    }

    public void feedAll() {
        System.out.println("Работник кормит животных:");
        worker.feed(duck, pork);
        worker.feed(wolf, clover);
        worker.feed(duck, clover);
        worker.feed(wolf, pork);
        worker.feed(horse, alfalfa);
        worker.feed(lion, beef);
        worker.feed(rabbit, oatmeal);
        worker.feed(fish, sheepmeat);
    }

    public void getVoices() {
        System.out.println("Животные говорят:");
        worker.getVoice(duck);
        worker.getVoice(horse);
        worker.getVoice(lion);
        worker.getVoice(rabbit);
        worker.getVoice(wolf);
    }

    public void letUsSwim() {
        System.out.println("Животные плавают:");
        Swim[] swimmers = {duck, fish};
        for (Swim animal : swimmers) {
            animal.swim();
        }
    }
}
