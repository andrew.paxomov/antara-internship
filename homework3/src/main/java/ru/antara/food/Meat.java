package ru.antara.food;

public abstract class Meat extends Food {

    public Meat(String name) {
        super(name);
    }
}
