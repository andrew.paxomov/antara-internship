package ru.antara.exception;

public class WrongFoodException extends Exception {

    public WrongFoodException(String message) {
        super(message);
    }

}
