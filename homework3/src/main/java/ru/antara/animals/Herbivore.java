package ru.antara.animals;

import ru.antara.enclosure.Size;
import ru.antara.exception.WrongFoodException;
import ru.antara.food.Food;
import ru.antara.food.Grass;

public abstract class Herbivore extends Animal {

    public Herbivore(String name) {
        super(name);
    }

    public Herbivore(String name, Size animalSize) {
        super(name, animalSize);
    }

    @Override
    public void eat(Food food) throws WrongFoodException {
        if (!(food instanceof Grass)) {
            throw new WrongFoodException(getName() + " не ест " + food.getName());
        }
        System.out.println(getName() + " ест " + food.getName());
    }
}
