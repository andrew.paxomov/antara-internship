package ru.antara.animals;

import ru.antara.enclosure.Size;

public class Fish extends Carnivorous implements Swim {

    public Fish() {
        super("Рыба", Size.SMALL);
    }

    @Override
    public void swim() {
        System.out.println(getName() + " плывёт");
    }
}
