package ru.antara.animals;

import ru.antara.enclosure.Size;

public class Wolf extends Carnivorous implements Run, Voice {

    public Wolf() {
        super("Волк", Size.MEDIUM);
    }

    @Override
    public void run() {
        System.out.println(getName() + " бежит");
    }

    @Override
    public String voice() {
        return getName() + " ууууууууу";
    }

}
