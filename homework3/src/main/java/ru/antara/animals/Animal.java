package ru.antara.animals;

import ru.antara.enclosure.Size;
import ru.antara.exception.WrongFoodException;
import ru.antara.food.Food;

import java.util.Objects;

public abstract class Animal {
    private final String name;
    private Size animalSize;

    public Animal(String name) {
        this.name = name;
    }

    public Animal(String name, Size animalSize) {
        this.name = name;
        this.animalSize = animalSize;
    }

    public abstract void eat(Food food) throws WrongFoodException;

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal that = (Animal) o;
        return Objects.equals(this.name, that.name) && this.animalSize == that.animalSize;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, animalSize);
    }

    public Size getAnimalSize() {
        return animalSize;
    }
}
