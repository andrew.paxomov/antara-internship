package ru.antara.animals;

import ru.antara.enclosure.Size;

public class Horse extends Herbivore implements Run, Voice {

    public Horse() {
        super("Лошадь", Size.LARGE);
    }

    @Override
    public void run() {
        System.out.println(getName() + " скачет");
    }

    @Override
    public String voice() {
        return getName() + " иго го";
    }
}
