package ru.antara.animals;

import ru.antara.enclosure.Size;

public class Rabbit extends Herbivore implements Run, Voice {

    public Rabbit() {
        super("Кролик", Size.SMALL);
    }

    @Override
    public void run() {
        System.out.println(getName() + " бежит");
    }

    @Override
    public String voice() {
        return getName() + " ви-ви";
    }
}
