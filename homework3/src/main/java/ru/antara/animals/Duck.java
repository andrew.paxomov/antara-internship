package ru.antara.animals;

import ru.antara.enclosure.Size;

public class Duck extends Herbivore implements Run, Swim, Fly, Voice {

    public Duck() {
        super("Утка", Size.SMALL);
    }

    @Override
    public void run() {
        System.out.println(getName() + " бежит");
    }

    @Override
    public void swim() {
        System.out.println(getName() + " плывёт");
    }

    @Override
    public void fly() {
        System.out.println(getName() + " летит");
    }

    @Override
    public String voice() {
        return getName() + " кря кря";
    }
}
