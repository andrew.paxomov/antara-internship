package ru.antara.animals;

import ru.antara.enclosure.Size;
import ru.antara.exception.WrongFoodException;
import ru.antara.food.Food;
import ru.antara.food.Meat;

public abstract class Carnivorous extends Animal {

    public Carnivorous(String name) {
        super(name);
    }

    public Carnivorous(String name, Size animalSize) {
        super(name, animalSize);
    }

    @Override
    public void eat(Food food) throws WrongFoodException {
        if (!(food instanceof Meat)) {
            throw new WrongFoodException(getName() + " не ест " + food.getName());
        }
        System.out.println(getName() + " ест " + food.getName());
    }
}
