package ru.antara.enclosure;

public enum Size {
    SMALL("маленький"),
    MEDIUM("средний"),
    LARGE("большой"),
    HUGE("огромный");

    private final String name;

    Size(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
