package ru.antara.enclosure;

import ru.antara.animals.Animal;

import java.util.HashSet;
import java.util.Set;

public class Enclosure<T extends Animal> {
    private Set<T> animals = new HashSet<>();
    private Size enclosureSize = Size.MEDIUM;

    public Enclosure(Set<T> animals) {
        this.animals = animals;
    }

    public Enclosure() {
    }

    public Enclosure(Size enclosureSize) {
        this.enclosureSize = enclosureSize;
    }

    public void addAnimal(T animal) {
        if (canAddAnimal(animal)) {
            animals.add(animal);
            System.out.println("Добавляем животное " + animal.getName() + " в вольер");
        } else System.out.println("Животное " + animal.getName() + " слишком большое для вольера");
    }

    public void deleteAnimal(T animal) {
        if (animals.contains(animal)) {
            System.out.println("Удаляем животное " + animal.getName() + " из вольера");
            animals.remove(animal);
        } else System.out.println("В вольере нет животного " + animal.getName() + ", удалить его нельзя");
    }

    public T getAnimal(String name) {
        for (T animal : animals) {
            if (animal.getName().equals(name)) return animal;
        }
        return null;
    }

    private boolean canAddAnimal(T animal) {
        return this.enclosureSize.ordinal() >= animal.getAnimalSize().ordinal();
    }
}