package ru.antara;

import ru.antara.animals.*;
import ru.antara.food.*;
import ru.antara.personal.Worker;

public class Zoo {

    public static void main(String[] args) {
        Duck duck = new Duck();
        Wolf wolf = new Wolf();
        Fish fish = new Fish();
        Horse horse = new Horse();
        Lion lion = new Lion();
        Rabbit rabbit = new Rabbit();

        Worker worker = new Worker();

        Alfalfa alfalfa = new Alfalfa();
        Beef beef = new Beef();
        Oatmeal oatmeal = new Oatmeal();
        Sheepmeat sheepmeat = new Sheepmeat();
        Clover clover = new Clover();
        Pork pork = new Pork();

        worker.feed(duck, pork);
        worker.feed(wolf, clover);
        worker.feed(duck, clover);
        worker.feed(wolf, pork);
        worker.feed(horse, alfalfa);
        worker.feed(lion, beef);
        worker.feed(rabbit, oatmeal);
        worker.feed(fish, sheepmeat);

        worker.getVoice(duck);
        worker.getVoice(horse);
        worker.getVoice(lion);
        worker.getVoice(rabbit);
        worker.getVoice(wolf);
        worker.getVoice(fish); // Здесь будет ошибка java: incompatible types

        Swim[] swimmers = {duck, fish};

        for (Swim animal : swimmers) {
            animal.swim();
        }
    }
}
