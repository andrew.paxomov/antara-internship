package ru.antara.animals;

public class Lion extends Carnivorous implements Run, Voice {

    public Lion() {
        super("Лев");
    }

    @Override
    public void run() {
        System.out.println(getName() + " бежит");
    }

    @Override
    public String voice() {
        return getName() + " рррр";
    }
}
