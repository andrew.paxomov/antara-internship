package ru.antara.animals;

public class Rabbit extends Herbivore implements Run, Voice {

    public Rabbit() {
        super("Кролик");
    }

    @Override
    public void run() {
        System.out.println(getName() + " бежит");
    }

    @Override
    public String voice() {
        return getName() + " ви-ви";
    }
}
