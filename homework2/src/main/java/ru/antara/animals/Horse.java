package ru.antara.animals;

public class Horse extends Herbivore implements Run, Voice {

    public Horse() {
        super("Лошадь");
    }

    @Override
    public void run() {
        System.out.println(getName() + " скачет");
    }

    @Override
    public String voice() {
        return getName() + " иго го";
    }
}
