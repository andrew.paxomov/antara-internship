package ru.antara.animals;

import ru.antara.food.Food;
import ru.antara.food.Grass;

public abstract class Herbivore extends Animal {

    public Herbivore(String name) {
        super(name);
    }

    @Override
    public void eat(Food food) {
        if (!(food instanceof Grass)) {
            System.out.println(getName() + " не ест " + food.getName());
            return;
        }
        System.out.println(getName() + " ест " + food.getName());
    }
}
