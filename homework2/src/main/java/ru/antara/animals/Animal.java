package ru.antara.animals;

import ru.antara.food.Food;

public abstract class Animal {
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public abstract void eat(Food food);

    public String getName() {
        return name;
    }
}
