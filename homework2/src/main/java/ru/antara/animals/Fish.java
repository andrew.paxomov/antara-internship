package ru.antara.animals;

public class Fish extends Carnivorous implements Swim {

    public Fish() {
        super("Рыба");
    }

    @Override
    public void swim() {
        System.out.println(getName() + " плывёт");
    }
}
