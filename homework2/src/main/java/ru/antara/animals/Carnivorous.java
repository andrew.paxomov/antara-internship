package ru.antara.animals;

import ru.antara.food.Food;
import ru.antara.food.Meat;

public abstract class Carnivorous extends Animal {

    public Carnivorous(String name) {
        super(name);
    }

    @Override
    public void eat(Food food) {
        if (!(food instanceof Meat)) {
            System.out.println(getName() + " не ест " + food.getName());
            return;
        }
        System.out.println(getName() + " ест " + food.getName());
    }
}
