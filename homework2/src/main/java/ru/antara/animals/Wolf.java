package ru.antara.animals;

public class Wolf extends Carnivorous implements Run, Voice {

    public Wolf() {
        super("Волк");
    }

    @Override
    public void run() {
        System.out.println(getName() + " бежит");
    }

    @Override
    public String voice() {
        return getName() + " ууууууууу";
    }

}
